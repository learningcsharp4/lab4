﻿namespace Lab_4
{
    public class Program 
    {
        static void Main()
        {
            Console.WriteLine("Ввод строки:");
            var inputString = Console.ReadLine();
            var numberPhone = new NumberPhone();
            var collectionNumberPhones = numberPhone.GetMatchCollection(inputString, numberPhone.putternNumberPhone);
            Console.WriteLine("Коллекция номеров телефонов:");
            numberPhone.FormatedOutput(collectionNumberPhones);
            var realNumber = new RealNumber();
            var collectionRealNumber = realNumber.GetMatchCollection(inputString, realNumber.putternRealNumber);
            Console.WriteLine("Коллекция вещественных чисел:");
            realNumber.FormatedOutput(collectionRealNumber);
            var date = new Date();
            var collectionDates = date.GetMatchCollection(inputString, date.putternDate);
            Console.WriteLine("Коллекция дат:");
            date.FormatedOutput(collectionDates);
            var time = new TimeData();
            var collectionTimes = time.GetMatchCollection(inputString, time.putternTime);
            Console.WriteLine("Коллекция времени:");
            time.FormatedOutput(collectionTimes);
        }
    }
}