﻿using System.Globalization;
using System.Text.RegularExpressions;

namespace Lab_4
{
    class RealNumber : IFinderByTempalete
    {
        #region Константы

        /// <summary>
        /// Регулярное выражение для поиска вещесвенных чисел.
        /// </summary>
        public string putternRealNumber = "\\-?\\d+[\\.|\\,]\\d+|\\d+";

        #endregion

        #region Поля и свойства

        /// <summary>
        /// Список вещественных чисел без повторяющихся.
        /// </summary>
        List<double> realNumbersNoDubles = new List<double>();

        #endregion

        #region Методы

        #region IFinderByTempalete

        public MatchCollection GetMatchCollection(string inputString, string puttern)
        {
            Regex regex = new Regex(puttern);
            MatchCollection matches = regex.Matches(inputString);
            return matches;
        }

        public void FormatedOutput(MatchCollection matchCollection)
        {
            IFormatProvider formatter = new NumberFormatInfo { NumberDecimalSeparator = "." };
            if (matchCollection.Count != 0)
            {
                foreach (var match in matchCollection)
                {
                    var matchString = match.ToString();
                    matchString = matchString.Replace(",", ".");                    
                    var realNumber = double.Parse(matchString, formatter);
                    realNumbersNoDubles.Add(realNumber);
                }
                realNumbersNoDubles = realNumbersNoDubles.Distinct().ToList();
                foreach (var realNumber in realNumbersNoDubles)
                {
                    string realNumberByDots = string.Format("{0:f}", realNumber);
                    realNumberByDots = realNumberByDots.Replace(",", ".");
                    Console.WriteLine(realNumberByDots);
                }
            }
            else
            {
                Console.WriteLine("Коллекция пуста.");
            }
        }

        #endregion

        #endregion
    }

}
