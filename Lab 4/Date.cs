﻿using System.Text.RegularExpressions;

namespace Lab_4
{
    class Date : IFinderByTempalete
    {
        #region Константы

        /// <summary>
        /// Регулярное выражение для поиска дат.
        /// </summary>
        public string putternDate = "\\d{2}\\.\\d{2}\\.\\d{4}|\\d{2}\\.\\d{4}\\.\\d{2}|\\d{4}\\.\\d{2}\\.\\d{2}";

        #endregion

        #region Поля и свойства

        /// <summary>
        /// Список дат без повторяющихся явные.
        /// </summary>
        List<DateTime> dateNoDubles = new List<DateTime>();
        /// <summary>
        /// Список дат без повторяющихся не явные.
        /// </summary>
        List<DateTime> dateNoDublesNotDefined = new List<DateTime>();

        #endregion

        #region IFinderByTempalete

        public MatchCollection GetMatchCollection(string inputString, string puttern)
        {
            Regex regex = new Regex(puttern);
            MatchCollection matches = regex.Matches(inputString);
            return matches;
        }

        public void FormatedOutput(MatchCollection matchCollection)
        {
            if (matchCollection.Count != 0)
            {
                foreach(var dates in matchCollection)
                {
                    var dateString = dates.ToString();
                    dateString = dateString.Replace(".", " ");
                    var dateSplitString = dateString.Split();
                    var list = new List<string>();
                    foreach(var dateSplit in dateSplitString)
                    {
                        if (dateSplit.Length == 4)
                        {
                            list.Add(dateSplit);
                        }
                        else
                        {
                            list.Insert(0, dateSplit);
                        }
                    }
                    if (!(int.Parse(list[0]) < 12 && int.Parse(list[0]) < 12))
                    {
                        var dataFormated = new DateTime(int.Parse(list[2]), int.Parse(list[1]), int.Parse(list[0]));
                        dateNoDubles.Add(dataFormated);
                    }
                    else
                    {
                        var dataFormated = new DateTime(int.Parse(list[2]), int.Parse(list[1]), int.Parse(list[0]));
                        dateNoDublesNotDefined.Add(dataFormated);
                    }
                }
                dateNoDubles = dateNoDubles.Distinct().ToList();
            }
            else
            {
                Console.WriteLine("Коллекция пуста.");
            }
            if (dateNoDubles.Count !=0)
            {
                Console.WriteLine("Ясно понятно:");
                foreach (var date in dateNoDubles)
                {
                    Console.WriteLine($"{date.ToString("d")}");
                }
            }
            if (dateNoDublesNotDefined.Count !=0)
            {
                Console.WriteLine("Не понятно:");
                foreach (var date in dateNoDublesNotDefined)
                {
                    Console.WriteLine($"{date.ToString("d")}");
                }
            }
        }
        #endregion
    }
}
