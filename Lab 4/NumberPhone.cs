﻿using System.Text.RegularExpressions;

namespace Lab_4
{
    class NumberPhone : IFinderByTempalete
    {
        #region Константы

        /// <summary>
        /// Шаблон регулярного выражения для выделения номеров телефонов.
        /// </summary>
        public string putternNumberPhone = "\\+?[7|8][ |-]\\(?\\d{3}\\)?[ |-]\\(?\\d{3}\\)?[ |-]\\(?\\d{2}\\)?[ |-]\\(?\\d{2}\\)?|\\+?[\\+7|8][ |-]\\d{10}|\\+?7\\d{10}|8\\d{10}";

        #endregion

        #region Поля и свойства

        /// <summary>
        /// Список номеров телефонов без повторяющихся.
        /// </summary>
        List<long> phoneNumbersNoDubles = new List<long>();

        #endregion

        #region Методы

        #region IFinderByTempalete

        public MatchCollection GetMatchCollection(string inputString, string puttern)
        {
            Regex regex = new Regex(puttern);
            MatchCollection matches = regex.Matches(inputString);
            return matches;
        }

        virtual public void FormatedOutput(MatchCollection matchCollection)
        {
            if (matchCollection.Count !=0)
            {
                foreach (var match in matchCollection)
                {
                    var matchString = match.ToString();
                    matchString = matchString.Replace(" ", "");
                    matchString = matchString.Replace("(", "");
                    matchString = matchString.Replace(")", "");
                    matchString = matchString.Replace("-", "");
                    if (matchString.Length==11)
                    {
                        matchString = matchString[1..];
                    }
                    else
                    {
                        matchString = matchString[2..];
                    }
                    long numberPhoneLong = long.Parse(matchString);
                    phoneNumbersNoDubles.Add(numberPhoneLong);

                }
                phoneNumbersNoDubles = phoneNumbersNoDubles.Distinct().ToList();
                foreach (var phoneNumber in phoneNumbersNoDubles)
                {
                    Console.WriteLine($"{phoneNumber:+7 (###) ### ## ##}");
                }
            }
            else 
            {
                Console.WriteLine("Коллекция пуста.");
            }           
        }
        #endregion

        #endregion
    }
}
