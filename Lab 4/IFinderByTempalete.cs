﻿using System.Text.RegularExpressions;

namespace Lab_4
{
    interface IFinderByTempalete
    {
        /// <summary>
        /// Метод поиска в строке совпадений по шаблону.
        /// </summary>
        /// <param name="inputString">Строка в которой ведется поиск.</param>
        /// <param name="puttern">Регулярное выражение для поиска.</param>
        /// <returns>Коллекция искомых объектов.</returns>
        MatchCollection GetMatchCollection(string inputString, string puttern);

        /// <summary>
        /// Вывод в консоль в соответсвии с формматом.
        /// </summary>
        /// <param name="matchCollection">Выводимые данные.</param>
        void FormatedOutput(MatchCollection matchCollection);       
    }
}
