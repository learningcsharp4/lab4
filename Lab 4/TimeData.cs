﻿using System.Text.RegularExpressions;

namespace Lab_4
{
    class TimeData : IFinderByTempalete
    {

        #region Константы

        /// <summary>
        /// Регулярное выражение для поиска времени.
        /// </summary>
        public string putternTime = "\\d{2}:\\d{2}:\\d\\d|\\d{2}:\\d{2}";

        #endregion

        #region Поля и свойства

        /// <summary>
        /// Список времен без повторяющихся.
        /// </summary>
        List<TimeOnly> TimeNoDubles = new List<TimeOnly>();

        #endregion

        #region IFinderByTempalete

        public MatchCollection GetMatchCollection(string inputString, string puttern)
        {
            Regex regex = new Regex(puttern);
            MatchCollection matches = regex.Matches(inputString);
            return matches;
        }

        public void FormatedOutput(MatchCollection matchCollection)
        {
            if (matchCollection.Count != 0)
            {
                foreach (Match time in matchCollection)
                {
                    var dateTime = time.ToString();
                    dateTime = dateTime.Replace(":", " ");
                    var dateSplitString = dateTime.Split();

                    var listRightTime = new List<string>();
                    if (int.Parse(dateSplitString[0]) <+24)
                    {
                        var rightTimeValue = true;
                        for (int i = 1; i < dateSplitString.Length; i++)
                        {
                            if (int.Parse(dateSplitString[i]) < 60) { rightTimeValue = rightTimeValue && (int.Parse(dateSplitString[i]) < 60); }
                        }
                        var timeFormated = new TimeOnly();
                        if (rightTimeValue)
                        {
                            if (dateSplitString.Length == 2)
                            {
                                timeFormated = new TimeOnly(int.Parse(dateSplitString[0]), int.Parse(dateSplitString[1]));
                            }
                            else
                            {
                                timeFormated = new TimeOnly(int.Parse(dateSplitString[0]), int.Parse(dateSplitString[1]), int.Parse(dateSplitString[2]));
                            }
                            TimeNoDubles.Add(timeFormated);
                        }
                    }
                }
                TimeNoDubles = TimeNoDubles.Distinct().ToList();
                foreach (var times in TimeNoDubles)
                {
                    Console.WriteLine(times.ToLongTimeString());
                }
            }
            else
            {
                Console.WriteLine("Коллекция пуста.");
            }
        }

        #endregion
    }
}
